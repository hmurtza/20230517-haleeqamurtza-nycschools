package com.example.nycschools.di

import com.example.nycschools.viewModel.SchoolViewModel
import dagger.Component

@Component(modules = [ApiModule::class])
interface ViewModelComponent {
    fun inject(viewModel: SchoolViewModel)
}