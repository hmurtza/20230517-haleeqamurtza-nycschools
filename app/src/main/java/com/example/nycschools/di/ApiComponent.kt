package com.example.nycschools.di

import com.example.nycschools.network.SchoolApiService
import dagger.Component

@Component(modules = [ApiModule::class])
interface ApiComponent {

    fun inject(service: SchoolApiService)
}