package com.example.nycschools.di

import com.example.nycschools.network.SchoolApi
import com.example.nycschools.network.SchoolApiService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://data.cityofnewyork.us/resource/"

@Module
open class ApiModule {

    @Provides
    open fun providesSchoolApi(): SchoolApi {
        return Retrofit.Builder().baseUrl(BASE_URL).addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(
                RxJava2CallAdapterFactory.create()
            ).build().create(SchoolApi::class.java)
    }

    @Provides
    open fun providesSchoolApiServices():SchoolApiService
    {
        return SchoolApiService()
    }
}