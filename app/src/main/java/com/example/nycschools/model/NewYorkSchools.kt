package com.example.nycschools.model

import com.google.gson.annotations.SerializedName

/*
Data class to hold list of schools
 */
data class NewYorkSchools(
    @SerializedName("dbn")
    var dbn: String?,
    @SerializedName("school_name")
    var school_name: String?,
    @SerializedName("neighborhood")
    var neighborhood: String?,
    @SerializedName("academicopportunities1")
    var academic_opportunities: String?
)
