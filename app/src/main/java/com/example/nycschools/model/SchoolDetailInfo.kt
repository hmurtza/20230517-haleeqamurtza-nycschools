package com.example.nycschools.model

import com.google.gson.annotations.SerializedName

/*
Data class to hold School SAT detail information
 */
data class SchoolDetailInfo(
    @SerializedName("dbn")
    var dbn: String?,
    @SerializedName("school_name")
    var school_name: String?,
    @SerializedName("num_of_sat_test_takers")
    var no_of_test: String?,
    @SerializedName("sat_critical_reading_avg_score")
    var reading_scores: String?,
    @SerializedName("sat_math_avg_score")
    var math_scores: String?,
    @SerializedName("sat_writing_avg_score")
    var writing_scores: String?,
)
