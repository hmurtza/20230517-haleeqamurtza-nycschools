package com.example.nycschools.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.nycschools.databinding.FragmentSchoolListBinding
import com.example.nycschools.model.NewYorkSchools
import com.example.nycschools.viewModel.SchoolViewModel


class SchoolListFragment : Fragment() {

    private lateinit var viewModel: SchoolViewModel
    private val schoolListAdapter = SchoolListAdapter(arrayListOf())
    private lateinit var binding: FragmentSchoolListBinding

    // updating list with data
    private val schoolListDataObserver = Observer<List<NewYorkSchools>> { schoolsList ->
        if(!schoolsList.isNullOrEmpty()){
            binding.schoolRecyclerView.visibility = View.VISIBLE
            schoolListAdapter.updateSchoolsList(schoolsList)
        }
    }
    private val isLoadingDataObserver = Observer<Boolean>
    { isLoading ->
        binding.progressBar.visibility = if (isLoading) View.VISIBLE else View.GONE
        if (isLoading) {
            binding.errorMessage.visibility = View.GONE
            binding.schoolRecyclerView.visibility = View.GONE
        }
    }
    private val isErrorDataObserver = Observer<Boolean> { isError ->
        binding.errorMessage.visibility = if (isError) View.VISIBLE else View.GONE

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSchoolListBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //initializing viewModel
        viewModel = ViewModelProvider(this)[SchoolViewModel::class.java]

        //observing livedata
        viewModel.schoolList.observe(viewLifecycleOwner, schoolListDataObserver)
        viewModel.isError.observe(viewLifecycleOwner, isErrorDataObserver)
        viewModel.isLoading.observe(viewLifecycleOwner, isLoadingDataObserver)
        viewModel.fetchSchoolList()

        binding.apply {
            schoolRecyclerView.apply {
                layoutManager = LinearLayoutManager(context)
                adapter = schoolListAdapter
            }
            refreshLayout.setOnRefreshListener {
                schoolRecyclerView.visibility = View.GONE
                errorMessage.visibility = View.GONE
                progressBar.visibility = View.VISIBLE
                viewModel.refresh()
                refreshLayout.isRefreshing = false
            }
        }
    }


}