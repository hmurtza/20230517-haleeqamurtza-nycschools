package com.example.nycschools.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.nycschools.R
import com.example.nycschools.databinding.FragmentSchoolDetailBinding
import com.example.nycschools.model.SchoolDetailInfo
import com.example.nycschools.viewModel.SchoolViewModel

class SchoolDetailFragment : Fragment() {

    private lateinit var databinding: FragmentSchoolDetailBinding
    private var viewModel = SchoolViewModel()
    private lateinit var schoolDbn: String


    private val schoolInfoDetailObserver = Observer<List<SchoolDetailInfo>> { detailInfo ->
        if (!detailInfo.isNullOrEmpty()) {
            setSchoolDetailInfo(detailInfo)
            databinding.detailScreenLayout.visibility = View.VISIBLE
        }
        else
        {
            showNoDataErrorBox()
        }
    }
    private val emptyDataObserver = Observer<Boolean> { isEmptyData ->
        if (!isEmptyData) {
            databinding.progressBar.visibility = View.GONE
            databinding.detailScreenLayout.visibility = View.VISIBLE
            databinding.emptyData.visibility = View.GONE
        } else {
            showNoDataErrorBox()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        databinding = FragmentSchoolDetailBinding.inflate(inflater, container, false)
        return databinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //receiving schoolDbn to send as query for SAT API
        schoolDbn = arguments?.getString("schoolDbn").toString()
        viewModel.fetchSchoolDetails(schoolDbn)

        //observing livedata
        viewModel.schoolInfoWithScores.observe(viewLifecycleOwner, schoolInfoDetailObserver)
        viewModel.emptyData.observe(viewLifecycleOwner, emptyDataObserver)

    }

    private fun setSchoolDetailInfo(schoolsDetailedInfo: List<SchoolDetailInfo>) {
        for (schools in schoolsDetailedInfo) {
            if (schoolDbn == schools.dbn) {
                databinding.apply {
                    schoolNameText.text = schools.school_name
                    writingScore.text = getString(R.string.sat_writing).plus(" ${ schools.writing_scores }")
                    readingScore.text = getString(R.string.sat_reading).plus(" ${ schools.reading_scores }")
                    mathScore.text = getString(R.string.sat_math).plus(" ${ schools.math_scores }")
                    noOfTestTakers.text = getString(R.string.sat_test_takers).plus(" ${ schools.no_of_test }")
                }
            } else {
                showNoDataErrorBox()
            }
        }
    }

    // displays error message if SAT Data is not present
    private fun showNoDataErrorBox() {
        databinding.apply {
            progressBar.visibility = View.GONE
            emptyData.visibility = View.VISIBLE
            detailScreenLayout.visibility = View.GONE
        }

    }

}