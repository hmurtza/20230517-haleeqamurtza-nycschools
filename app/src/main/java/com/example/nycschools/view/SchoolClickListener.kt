package com.example.nycschools.view

import android.view.View

interface SchoolClickListener {
    fun onClick(v:View)
}