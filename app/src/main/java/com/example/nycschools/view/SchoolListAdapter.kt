package com.example.nycschools.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.nycschools.R
import com.example.nycschools.databinding.ItemSchoolListBinding
import com.example.nycschools.model.NewYorkSchools

class SchoolListAdapter(
    private val newYorkSchoolList: ArrayList<NewYorkSchools>
) :
    RecyclerView.Adapter<SchoolListAdapter.SchoolViewHolder>(), SchoolClickListener {

    fun updateSchoolsList(newSchoolList: List<NewYorkSchools>) {
        newYorkSchoolList.clear()
        newYorkSchoolList.addAll(newSchoolList)
        notifyDataSetChanged()
    }

    override fun onBindViewHolder(holder: SchoolViewHolder, position: Int) {
        holder.view.schools = newYorkSchoolList[position]
        holder.view.listener = this
    }

    override fun onClick(v: View) {
        for (schools in newYorkSchoolList) {
            if (v.tag == schools.school_name) {
                v.findNavController()
                    .navigate(R.id.action_schoolListFragment_to_schoolDetailFragment,
                        Bundle().apply {
                            putString("schoolDbn", schools.dbn)
                        }
                    )
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SchoolViewHolder {
        val view = ItemSchoolListBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return SchoolViewHolder(view)
    }

    override fun getItemCount(): Int {
        return newYorkSchoolList.size
    }

    class SchoolViewHolder(var view: ItemSchoolListBinding) : RecyclerView.ViewHolder(view.root)

}