package com.example.nycschools.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.nycschools.di.DaggerViewModelComponent
import com.example.nycschools.model.NewYorkSchools
import com.example.nycschools.model.SchoolDetailInfo
import com.example.nycschools.network.SchoolApiService
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class SchoolViewModel : ViewModel() {

    @Inject
    lateinit var schoolApiService:SchoolApiService
    private val disposable = CompositeDisposable()

    val schoolList by lazy {
        MutableLiveData<List<NewYorkSchools>>()
    }
    val schoolInfoWithScores by lazy {
        MutableLiveData<List<SchoolDetailInfo>>()
    }
    val isLoading by lazy {
        MutableLiveData<Boolean>()
    }
    val isError by lazy {
        MutableLiveData<Boolean>()
    }
    val emptyData by lazy {
        MutableLiveData<Boolean>()
    }
    init {
        DaggerViewModelComponent.create().inject(this)
    }

    fun fetchSchoolList() {
        getNewYorkSchoolsList()
    }

    fun fetchSchoolDetails(dbn:String)
    {
        getSchoolsWithScores(dbn)
    }

    private fun getNewYorkSchoolsList() {
        disposable.add(
            schoolApiService.getSchoolList().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<NewYorkSchools>>() {
                    override fun onSuccess(schoolsList: List<NewYorkSchools>) {
                        isError.value = false
                        schoolList.value = schoolsList
                        isLoading.value = false
                    }

                    override fun onError(e: Throwable) {
                        e.printStackTrace()
                        isLoading.value = false
                        schoolList.value = null
                        isError.value = true
                    }


                })
        )
    }

    private fun getSchoolsWithScores(dbn: String) {
        disposable.add(
            schoolApiService.getSchoolWithScores(dbn=dbn).subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableSingleObserver<List<SchoolDetailInfo>>() {
                    override fun onSuccess(schoolDetailInfo: List<SchoolDetailInfo>) {
                        emptyData.value= false
                        schoolInfoWithScores.value = schoolDetailInfo
                    }

                    override fun onError(e: Throwable) {
                        Log.e("Error",e.toString())
                        emptyData.value= true
                    }

                })
        )
    }

    fun refresh() {
        isLoading.value = true
        getNewYorkSchoolsList()
    }

    override fun onCleared() {
        super.onCleared()
        disposable.dispose()
    }

}