package com.example.nycschools.network

import com.example.nycschools.model.NewYorkSchools
import com.example.nycschools.model.SchoolDetailInfo
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface SchoolApi {
    @GET("s3k6-pzi2")
    fun getNewYorkSchoolsList(): Single<List<NewYorkSchools>>

    @GET("f9bf-2cp4")
    fun getSchoolDetailsWithScores(@Query("dbn") dbn: String): Single<List<SchoolDetailInfo>>

}