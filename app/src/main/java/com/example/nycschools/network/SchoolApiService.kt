package com.example.nycschools.network

import com.example.nycschools.di.DaggerApiComponent
import com.example.nycschools.model.NewYorkSchools
import com.example.nycschools.model.SchoolDetailInfo
import io.reactivex.Single
import javax.inject.Inject

open class SchoolApiService {

    @Inject
    lateinit var schoolApi:SchoolApi

    init {
        DaggerApiComponent.create().inject(this)
    }


    fun getSchoolList(): Single<List<NewYorkSchools>> {
        return schoolApi.getNewYorkSchoolsList()
    }

    fun getSchoolWithScores(dbn:String): Single<List<SchoolDetailInfo>> {
        return schoolApi.getSchoolDetailsWithScores(dbn = dbn)
    }

}