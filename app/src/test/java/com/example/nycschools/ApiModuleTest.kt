package com.example.nycschools

import com.example.nycschools.di.ApiModule
import com.example.nycschools.network.SchoolApiService

class ApiModuleTest(var mockSchoolApiService: SchoolApiService): ApiModule() {
    override fun providesSchoolApiServices(): SchoolApiService {
        return mockSchoolApiService
    }
}