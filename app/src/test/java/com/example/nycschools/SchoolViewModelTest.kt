package com.example.nycschools

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.example.nycschools.di.DaggerViewModelComponent
import com.example.nycschools.model.NewYorkSchools
import com.example.nycschools.network.SchoolApi
import com.example.nycschools.network.SchoolApiService
import com.example.nycschools.viewModel.SchoolViewModel
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.internal.schedulers.ExecutorScheduler
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import org.mockito.MockitoAnnotations
import org.mockito.junit.MockitoJUnit
import org.mockito.junit.MockitoRule


class SchoolViewModelTest {
    @get:Rule
    var mockitoRule: MockitoRule? = MockitoJUnit.rule()

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()//For LiveData

    @Mock
    private lateinit var schoolApiService: SchoolApiService

    @Mock
    private lateinit var schoolApi: SchoolApi

    private var schoolViewModel = SchoolViewModel()

    @Before
    fun setupRxSchedulers() {
        val immediate = object : Scheduler() {
            override fun createWorker(): Worker {
                return ExecutorScheduler.ExecutorWorker({ it.run() }, true)
            }
        }

        RxJavaPlugins.setInitNewThreadSchedulerHandler { immediate }
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { immediate }
    }

    @Before
    fun setUp() {
        MockitoAnnotations.openMocks(this)

        DaggerViewModelComponent.builder()
            .apiModule(ApiModuleTest(schoolApiService))
            .build()
            .inject(schoolViewModel)

        schoolApiService.schoolApi = schoolApi
    }

    @Test
    fun getSchoolListApiSuccess() {

        val schools = NewYorkSchools(
            dbn = "dbn",
            school_name = "schoolName",
            neighborhood = "newyork",
            academic_opportunities = "none"
        )
        val listOfSchools = listOf(schools)
        val testSingle = Single.just(listOfSchools)
        Mockito.`when`(schoolApiService.getSchoolList()).thenReturn(testSingle)

        schoolViewModel.refresh()

        Assert.assertEquals(
            1, schoolViewModel.schoolList
                .value?.size
        )
        Assert.assertEquals(
            false, schoolViewModel.isError
                .value
        )
        Assert.assertEquals(false, schoolViewModel.isLoading.value)
    }

    @Test
    fun getSchoolListApiFailure() {
        val testSingle = Single.error<List<NewYorkSchools>>(Throwable())

        Mockito.`when`(schoolApiService.getSchoolList()).thenReturn(testSingle)

        schoolViewModel.refresh()

        Assert.assertEquals(
            null, schoolViewModel.schoolList
                .value
        )
        Assert.assertEquals(
            true, schoolViewModel.isError
                .value
        )
        Assert.assertEquals(false, schoolViewModel.isLoading.value)
    }

}